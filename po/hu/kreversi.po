# KTranslator Generated File

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Csaba Major <major@terrasoft.hu>, 1998.
#
#
msgid ""
msgstr ""
"Project-Id-Version: KReversi 0.6.5\n"
"POT-Creation-Date: 1998-10-01 13:21+0200\n"
"PO-Revision-Date: 1998-04-20 18:43+0100\n"
"Last-Translator: Csaba Major <major@terrasoft.hu>\n"
"Language-Team: Hungarian <hu@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=Latin2\n"
"Content-Transfer-Encoding: ISO-8859-2\n"

# app.cpp:208 app.cpp:506
#: app.cpp:218 app.cpp:553
msgid ""
"A problem with the sound server occured!\n"
"Cannot enable sound support."
msgstr ""
"Hiba t�rt�nt a hangrendszer el�r�sekor!\n"
"Nem lehet a hangokat enged�lyezni."

# app.cpp:240
#: app.cpp:245
msgid "&New game"
msgstr "�&j j�t�k"

# app.cpp:240
#: app.cpp:246
#
msgid "&Load game"
msgstr "J�t�k bet�&lt�s"

# app.cpp:240
#: app.cpp:247
#
msgid "&Save game"
msgstr "J�t�k ment�&s"

# app.cpp:235
#: app.cpp:252
#
msgid "Get &hint"
msgstr "&Tipp a k�vetkez� l�p�shez"

# app.cpp:237
#: app.cpp:254
msgid "&Stop thinking"
msgstr "&V�rj!"

# app.cpp:238
#: app.cpp:255
msgid "&Continue"
msgstr "&Folytat�s"

# app.cpp:242
#: app.cpp:257
msgid "&Undo move"
msgstr "L�p�s &visszavon�sa"

# app.cpp:243
#: app.cpp:258
msgid "Switch si&des"
msgstr "A k�t oldal &felcser�l�se"

# app.cpp:245
#: app.cpp:260
msgid "Hall Of &Fame..."
msgstr "Top &lista"

# app.cpp:251
#: app.cpp:266
msgid "Level 1 (Wimp)"
msgstr "1. szint (amat�r)"

# app.cpp:252
#: app.cpp:267
msgid "Level 2 (Beginner)"
msgstr "2. szint (kezd�)"

# app.cpp:253
#: app.cpp:268
msgid "Level 3 (Novice)"
msgstr "3. szint (kezd�)"

# app.cpp:254
#: app.cpp:269
msgid "Level 4 (Average)"
msgstr "4. szint (�tlagos)"

# app.cpp:255
#: app.cpp:270
msgid "Level 5 (Good)"
msgstr "5. szint (j�)"

# app.cpp:256
#: app.cpp:271
msgid "Level 6 (Expert)"
msgstr "6. szint (profi)"

# app.cpp:257
#: app.cpp:272
msgid "Level 7 (Master)"
msgstr "7. szint (nagymester)"

# app.cpp:258
#: app.cpp:273
msgid "Skill"
msgstr "�gyess�g"

# app.cpp:260
#: app.cpp:275
msgid "&Shrink board"
msgstr "T�bla &kicsiny�t�se"

# app.cpp:261
#: app.cpp:276
msgid "&Enlarge board"
msgstr "T�bla &n�vel�se"

# app.cpp:265
#: app.cpp:280
msgid "&Half size"
msgstr "&F�l m�ret"

# app.cpp:266
#: app.cpp:281
#, c-format
msgid "60%"
msgstr "60%"

# app.cpp:267
#: app.cpp:282
#, c-format
msgid "80%"
msgstr "80%"

# app.cpp:268
#: app.cpp:283
msgid "D&efault size"
msgstr "&Alap m�ret"

# app.cpp:269
#: app.cpp:284
#, c-format
msgid "120%"
msgstr "120%"

# app.cpp:270
#: app.cpp:285
#, c-format
msgid "140%"
msgstr "140%"

# app.cpp:271
#: app.cpp:286
#, c-format
msgid "160%"
msgstr "160%"

# app.cpp:272
#: app.cpp:287
#, c-format
msgid "180%"
msgstr "180%"

# app.cpp:273
#: app.cpp:288
msgid "&Double size"
msgstr "&Dupla m�ret"

# app.cpp:274
#: app.cpp:289
msgid "Set size"
msgstr "M�ret be�ll�t�sa"

# app.cpp:277
#: app.cpp:292
msgid "Select &background color..."
msgstr "&H�tt�rsz�n kiv�laszt�sa..."

# app.cpp:283
#: app.cpp:298
msgid "none"
msgstr "semmi"

# app.cpp:293
#: app.cpp:308
msgid "Select background image"
msgstr "H�tt�rk�p kiv�laszt�sa"

# app.cpp:294
#: app.cpp:309
msgid "&Grayscale"
msgstr "&Sz�rke�rnyalat"

# app.cpp:296
#: app.cpp:311
msgid "&Animations"
msgstr "&Anim�ci�k"

# app.cpp:300
#: app.cpp:315
msgid "1 (fastest)"
msgstr "1 (leggyorsabb)"

# app.cpp:306
#: app.cpp:321
msgid "10 (slowest)"
msgstr "10 (leglassabb)"

# app.cpp:307
#: app.cpp:322
msgid "Animation speed"
msgstr "Anim�ci� sebess�ge"

# app.cpp:310
#: app.cpp:325
msgid "S&ound"
msgstr "&Hang"

# app.cpp:357
#: app.cpp:369
msgid "Stop thinking"
msgstr "V�rj!"

# app.cpp:358
#: app.cpp:370
msgid "Undo move"
msgstr "L�p�s visszavon�sa"

# app.cpp:359
#: app.cpp:371
msgid "Shrink board"
msgstr "T�bla kicsiny�t�se"

# app.cpp:360
#: app.cpp:372
msgid "Enlarge board"
msgstr "T�bla n�vel�se"

# app.cpp:361
#: app.cpp:373
msgid "Get hint"
msgstr "Tipp a k�vetkez� l�p�shez"

# app.cpp:362
#: app.cpp:374
msgid "Get help"
msgstr "S�g�"

# app.cpp:369
#: app.cpp:381
msgid "XXXXX's turn"
msgstr "XXXXX k�vetkezik"

# app.cpp:370
#: app.cpp:382
msgid "You (XXXXX): 88"
msgstr "Te (XXXXX): 88"

# app.cpp:371
#: app.cpp:383
msgid "Computer (XXXXX): 88"
msgstr "G�p (XXXXX): 88"

# app.cpp:629 app.cpp:641 app.cpp:661
#: app.cpp:429
#
msgid "Game saved"
msgstr "J�t�k elmentve"

# app.cpp:556
#: app.cpp:591
msgid "not yet implemented"
msgstr "m�g nem m�k�dik"

# app.cpp:569 app.cpp:574
#: app.cpp:604 app.cpp:609
#, c-format
msgid "You (%s): %d"
msgstr "Te (%s): %d"

# app.cpp:570 app.cpp:574 app.cpp:841
#: app.cpp:605 app.cpp:612 app.cpp:912
msgid "blue"
msgstr "k�k"

# app.cpp:571 app.cpp:576
#: app.cpp:606 app.cpp:611
#, c-format
msgid "Computer (%s): %d"
msgstr "G�p (%s): %d"

# app.cpp:571 app.cpp:573 app.cpp:843
#: app.cpp:607 app.cpp:610 app.cpp:914
msgid "red"
msgstr "piros"

# app.cpp:618
#: app.cpp:656
msgid "End of game"
msgstr "V�ge a j�t�knak"

# app.cpp:628
#: app.cpp:666
#, c-format
msgid ""
"Game is drawn!\n"
"\n"
"You     : %d\n"
"Computer: %d"
msgstr ""
"D�ntetlen!\n"
"\n"
"Te        : %d\n"
"G�p       : %d"

# app.cpp:629 app.cpp:641 app.cpp:661
#: app.cpp:667 app.cpp:679 app.cpp:699
msgid "Game ended"
msgstr "V�ge a j�t�knak"

# app.cpp:639
#: app.cpp:677
msgid ""
"Congratulations, you have won!\n"
"\n"
"You     : %d\n"
"Computer: %d\n"
"Your rating %4.1f%%"
msgstr ""
"Gratul�lok, Te nyert�l!\n"
"\n"
"Te        : %d\n"
"Sz�m�t�g�p: %d\n"
"Eredm�nyed %4.1f%%"

# app.cpp:659
#: app.cpp:697
#, c-format
msgid ""
"You have lost the game!\n"
"\n"
"You     : %d\n"
"Computer: %d"
msgstr ""
"Elvesz�tetted a j�t�kot!\n"
"Te        : %d\n"
"Sz�m�t�g�p: %d"

# app.cpp:670
#: app.cpp:708
msgid "Red's"
msgstr "Piros"

# app.cpp:670 app.cpp:672
#: app.cpp:708 app.cpp:710
msgid "turn"
msgstr "l�p�se"

# app.cpp:672
#: app.cpp:710
msgid "Blue's"
msgstr "K�k"

# app.cpp:811
#: app.cpp:855
msgid "Hall of Fame"
msgstr "Top lista"

# app.cpp:814 app.cpp:816
#: app.cpp:856 app.cpp:860
msgid "KReversi: Hall Of Fame"
msgstr "KReversi: Top Lista"

#: app.cpp:881
msgid "Rank"
msgstr "Helyez�s"

# app.cpp:321
#: app.cpp:885
#
msgid "Name"
msgstr "N�v"

# app.cpp:812
#: app.cpp:889
#
msgid "Color"
msgstr "Sz�n"

#: app.cpp:893
msgid "Score"
msgstr "Pontsz�m"

#: app.cpp:897
msgid "Rating"
msgstr "Oszt�lyoz�s"

# app.cpp:866
#: app.cpp:987
#
msgid ""
"You've made in into the \"Hall Of Fame\".Type in\n"
"your name so mankind will always remember\n"
"your cool rating."
msgstr ""
"Beker�lt�l a \"Top List�ra\". G�peld be a neved, hogy az emberis�g �r�kk� "
"eml�kezhessen a j� helyez�sedre!"

# app.cpp:871
#: app.cpp:990
msgid "Your name:"
msgstr "Neved:"

# about.cpp:52
#: about.cpp:52
#
msgid "About kreversi"
msgstr "KReversi n�vjegye"

# about.cpp:67
#: about.cpp:65
msgid "Version "
msgstr "Verzi� "

# about.cpp:69
#: about.cpp:67
#
msgid ""
"\n"
"(c) 1997 Mario Weilguni <mweilguni@sime.com>\n"
"\n"
"This program is free software\n"
"published under the GNU General\n"
"Public License (take a look\n"
"into help for details)\n"
"\n"
"Thanks to:\n"
"\tMats Luthman for the game engine\n"
"\t(I've ported it from his JAVA applet)\n"
"\n"
"\tStephan Kulow\n"
"\tfor comments and bugfixes\n"
"\n"
"\tArne Klaassen\n"
"\t for the raytraced chips"
msgstr "\n"
"(c) 1997 Mario Weilguni <mweilguni@sime.com>\n"
"\n"
"Ez a program szabad szoftver, �s a\n"
"GNU General Public License al� tartozik.\n"
"(n�zd meg a seg�ts�get a r�szletek�rt)\n"
"\n"
"K�sz�net:\n"
"\tMats Luthmannak a j�t�k k�dj��rt\n"
"\t(Az � JAVA appletj�b�l szedtem)\n"
"\n"
"\tStephan Kulownak\n"
"\taz �p�t� megjegyz�sei�rt �s jav�t�sai�rt\n"
"\n"
"\tArne Klaassennek\n"
"\t a raytracelt chipek�rt"

# app.cpp:207 app.cpp:505
#~ msgid "Error"
#~ msgstr "Hiba"

# app.cpp:232
#~ msgid "&Quit"
#~ msgstr "&Kil�p�s"

# app.cpp:314 app.cpp:324
#~ msgid "&Help"
#~ msgstr "&S�g�"

# app.cpp:315
#~ msgid "&Rules"
#~ msgstr "S&zab�lyok"

# app.cpp:316
#~ msgid "&Strategy hints"
#~ msgstr "S&tart�giai �tletek"

# app.cpp:318
#~ msgid "&About..."
#~ msgstr "&N�vjegy..."

# app.cpp:320
#~ msgid "&File"
#~ msgstr "&File"

# app.cpp:322
#~ msgid "&Options"
#~ msgstr "&Opci�k"

# app.cpp:556
#~ msgid "Information"
#~ msgstr "Inform�ci�"

# about.cpp:83 app.cpp:880
#~ msgid "Ok"
#~ msgstr "Ok"